import React from 'react';
import { View, Image, Text, TouchableHighlight, StyleSheet } from 'react-native';
//import { Colors } from 'react-native/Libraries/NewAppScreen';



const styles = StyleSheet.create({
    titulo: {
        fontSize: 30,
        fontWeight: 'bold',
        //fontStyle:'italic',
        fontFamily: 'sans-serif-light',
        color: 'orange',
        textAlign: 'center',
        marginLeft: 'auto',
        textShadowOffset: { width: 2, height: 1 },
        textShadowRadius: 2,
        textShadowColor: '#000',
        //position:'absolute',
        //right:0,
        //left: 70,

    },
})

class TitleApp extends React.Component {
    render() {
        return (
            <View style={{flexDirection: 'row',left: 70 }} >

                <Image source={require('../assets/wheat.png')}
                    style={{marginLeft: 'auto',marginTop:2 ,width: 40, height: 40 }}
                />
                <Text style={styles.titulo} >
                    Birrapp
                </Text>

            </View>



        )
    }
}

export default TitleApp;