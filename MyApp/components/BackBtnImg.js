import React, { Component } from 'react';
import { Image, StyleSheet} from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import { Icon } from 'react-native-elements';

const styles = StyleSheet.create({
    img: {
        resizeMode: 'center',
        width: 150,
        height: 150,
        marginTop: 20,
        color:'#000',
        backgroundColor:'#000'
    },
})

const iconSize = 35;

class BackButton extends Component {
  
  render() {
    return <Icon
      name='angle-left'
      type='font-awesome'
      size={50}
      color='#000'
    />
    /*return <Ionicons
        name="arrow-undo-outline"
        color="#000"
        size={iconSize}
      //style={styles.menuIcon}         
    />*/
  }
}

export default BackButton ;