import React, { Component } from 'react';
import { Image, Dimensions} from 'react-native';

var widthVar = Dimensions.get('window').width; //full width
var heightVar = 180;

class ImageBirreria extends Component {
  
  render() {
   
    return <Image
          source={{uri:this.props.url}}
          style={{ width: widthVar, height: heightVar}}
        /> 
  }
}

export default ImageBirreria