import React from 'react';
import { StyleSheet,TouchableOpacity } from 'react-native';
import { Ionicons } from '@expo/vector-icons';

const styles = StyleSheet.create({
    menuIcon: {
        //zIndex: 9,
        //position: 'absolute',
        //top: 8,
        left:12
    },
})

class MenuButton extends React.Component {
    render() {
        return (
            <TouchableOpacity   onPress={() => { this.props.openDrawer() }}>
                <Ionicons
                    name="md-menu"
                    color="#fff"
                    size={40}
                    style={styles.menuIcon}
                  
                />
            </TouchableOpacity>
        
        )
    }
}

export default MenuButton;