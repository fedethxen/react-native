import React from 'react';
import {Text} from 'react-native';

import { createAppContainer, createSwitchNavigator } from 'react-navigation';

import DrawerNav from './DrawerNav';

export default createAppContainer(
    createSwitchNavigator({
      // You could add another route here for authentication.
      // Read more at https://reactnavigation.org/docs/en/auth-flow.html
      Main: DrawerNav,
    })
  );
  
/*
const AppContainer = createAppContainer(DrawerNav);

export default AppContainer;
*/

