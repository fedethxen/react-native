//This is an example of Tab inside Navigation Drawer in React Native//
import React from 'react';
//import react in our code.
import { createAppContainer } from 'react-navigation'
import { createStackNavigator} from 'react-navigation-stack';
import { createBottomTabNavigator , createMaterialTopTabNavigator, MaterialTopTabBar} from 'react-navigation-tabs';

import { Ionicons } from '@expo/vector-icons';
 
//Import all the screens for Tab
import DetailsScreen from '../screens/DetailsScreen';
import ScroolScreen from '../screens/ScroolScreen';
import GeolocationScreen from '../screens/GeolocationScreen';
import PromotionScreen from '../screens/PromotionScreen';
import HorariosScreen from '../screens/HorariosScreen';

const iconSize = 35;

const  TabScreen =  createMaterialTopTabNavigator (
  {
    Details: { 
        screen: DetailsScreen  ,
        navigationOptions: () => ({
          tabBarLabel:'Info',
          tabBarIcon:({tintColor})=>(
            <Ionicons
              name="md-information-circle"
              color={tintColor}
              size={iconSize}
            //style={styles.menuIcon}         
           />
          ) 
        })
    },
    Scroll: { 
      screen: ScroolScreen,
      navigationOptions: () => ({
        tabBarLabel:'Detalle',
        tabBarIcon:({tintColor})=>(
          <Ionicons
            //name="md-finger-print"
            name="md-umbrella"
            color={tintColor}
            size={iconSize}
          //style={styles.menuIcon}         
         />
        )
      })
    },
    Geolocation: { 
      screen: GeolocationScreen,
      navigationOptions: () => ({
        tabBarLabel:'Detalle',
        tabBarIcon:({tintColor})=>(
          <Ionicons
            name="md-pin"
            color={tintColor}
            size={iconSize}
          //style={styles.menuIcon}         
         />
        )
      })
    },
    Horarios: { 
      screen: HorariosScreen,
      navigationOptions: () => ({
        tabBarLabel:'Detalle',
        tabBarIcon:({tintColor})=>(
          <Ionicons
            name="md-time"
            color={tintColor}
            size={iconSize}
          //style={styles.menuIcon}         
         />
        )
      })
    },
    Promos: { 
      screen: PromotionScreen,
      navigationOptions: () => ({
        tabBarLabel:'Detalle',
        tabBarIcon:({tintColor})=>(
          <Ionicons
            name="md-pricetags"
            color={tintColor}
            size={iconSize}
          //style={styles.menuIcon}         
         />
        )
      })
    },
  },
  {
    initialRouteName: 'Details',
    tabBarPosition:"top",
    swipeEnabled: true,
    animationEnabled: true,
    tabBarOptions: {
      showIcon:true,
      showLabel:false,
      inactiveTintColor: '#646464',
      activeTintColor: '#022d02',
      /*tabStyle:{
        backgroundColor:"#000",
        //height:45,
      },*/
      style:{
        backgroundColor:'#FFF',
        height:55
      },
      iconStyle:{
        justifyContent:'center',
        alignItems:'center',
        width:40,
        height:40,
        bottom:5
      },
      /*labelStyle: {
        textAlign: 'center',
      },*/
      indicatorStyle: {
        borderTopColor: '#022d02',
        borderTopWidth: 4,
      },
    },
  }
);

export default TabScreen;

