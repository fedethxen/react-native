import React from 'react';
import {Text,Image} from 'react-native';

import { createDrawerNavigator } from 'react-navigation-drawer';

//import HomeScreen from '../screens/HomeScreen';
import DetailsScreen from '../screens/DetailsScreen';
import ScroolScreen from '../screens/ScroolScreen';
import FetchExample from '../screens/OtherScreen';

import MyAccount from '../screens/MyAccount/MyAccount';

//import AccountStack from './AccountStack';

import StackNav from './StackNav';



//npm install -g expo-cli


const DrawerNav = createDrawerNavigator({
  Home: {
    screen: StackNav,
    navigationOptions: () => ({
      drawerIcon: ()=> 
      (
        <Image source={require('../assets/pitcher.png')} style={{width: 30, height: 30 }}/>
      )
    })
  },
  Details: {
    screen: DetailsScreen,
    navigationOptions: () => ({
      drawerIcon: ()=> 
      (
        <Image source={require('../assets/beer.png')} style={{width: 30, height: 30 }}/>
      )
    })
  }, 
  Scrool:{
    screen:ScroolScreen,
    navigationOptions: () => ({
      drawerIcon: ()=> 
      (
        <Image source={require('../assets/beerbarril.png')} style={{width: 30, height: 30 }}/>
      )
    })
  },
  Fetch:{
    screen:FetchExample,
    navigationOptions: () => ({
      drawerIcon: ()=> 
      (
        <Image source={require('../assets/beer-tap.png')} style={{width: 30, height: 30 }}/>
      )
    })
  },  
  Account:{
    screen:MyAccount,
    navigationOptions: () => ({
      drawerIcon: ()=> 
      (
        <Image source={require('../assets/account.png')} style={{width: 30, height: 30 }}/>
      )
    })
  } 
  
})

export default DrawerNav;