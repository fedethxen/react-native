import React from 'react';
import {Text} from 'react-native';

import { createStackNavigator} from 'react-navigation-stack';

import HomeScreen from '../screens/HomeScreen';
import DetailsScreen from '../screens/DetailsScreen';
import ScroolScreen from '../screens/ScroolScreen';
import FetchExample from '../screens/OtherScreen';
import BirreriaScreen from '../screens/BirreriaScreen'


import MyAccount from '../screens/MyAccount/MyAccount';
import Register from '../screens/MyAccount/Register';

import MenuButton from '../components/MenuButton';
import TitleApp from '../components/TituloApp';
import BackButton from '../components/BackBtnImg';
import TabScreen from './TabInScreen';


const StackNav = createStackNavigator(
  {
    Home: {
      screen: HomeScreen,
      navigationOptions: ({navigation}) => ({
        headerTitle:()=> ( 
          <TitleApp />
        ),
        headerLeft: ()=> ( 
          <MenuButton {...navigation}/>
        ),
        headerStyle: {
            //backgroundColor: '#003f5c',
            backgroundColor: '#022d02',
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
          fontWeight: 'bold',
        }

      })
    },
    Details: {
      screen: DetailsScreen,
      /*navigationOptions: () => ({
        headerTitle: 'Details',
      })*/
    }, 
    Scrool:{
      screen:ScroolScreen,
      /*navigationOptions: () => ({
        headerTitle: 'Scroll',
      })*/
    } , 
    Fetch:{
      screen:FetchExample,
      navigationOptions: () => ({
        headerTitle: 'MARVEL (APIRest Conexion)',
      })
    },
    Birreria:{
      screen:BirreriaScreen,
      navigationOptions: () => ({
        headerTransparent: true,
        headerBackImage:()=> ( 
          <BackButton
            //styles={{width:15,height:15}}
          />
        ),
        headerTitle:'',
        /*headerStyle: { 
          backgroundColor: '#000000',
          opacity:0.5,
        
        },    
        headerTintColor:'#fff'    */
      })  
    },
    Tab: {
      screen: TabScreen,
      navigationOptions: ({navigation}) => ({
        headerTitle: navigation.getParam('name', 'Nombre Birreria'),
        headerStyle: {
          
            backgroundColor: '#022d02',        
        },  
        headerTintColor:'#fff'      
      })

    },
    Account:{
      screen:MyAccount,
      navigationOptions: () => ({
        headerTitle: 'Account',
        headerStyle: {
          
            backgroundColor: '#003f5c',        
        },  
        headerTintColor:'#fff'      
      })
    },
    Register: {
      screen: Register,
      navigationOptions: () => ({
          headerTitle: 'Registro de Usuario',
      })
    },  

  },
  {
    initialRouteName: 'Home',
  }
    
);

export default StackNav;

