import React from 'react';

import { View, Button, Text, StyleSheet } from 'react-native';

class MyAccount extends React.Component {

    componentDidMount(){
        console.log(this);
        
    }

    render(){
        return(
            <View style={styles.container}>
                <Text style={styles.text}>Mi cuenta o login</Text>
                <Button 
                    style={styles.text}
                    title='registro '
                    onPress={() => 
                        /* 1. Navigate to the Details route with params */
                        this.props.navigation.navigate('Register')
                    } 
                />
            </View>

        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems:'center',
        justifyContent:'center',
        backgroundColor:'#003f5c',

    },
    text:{
        color:"#fff"
    }
})

export default MyAccount