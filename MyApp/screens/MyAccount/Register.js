import React from 'react';
import { View, Button, Text, StyleSheet } from 'react-native';

class Register extends React.Component {
    
    render(){
        return(
            <View style={styles.container}>
                <Text style={styles.text}>Registro de Usuario</Text>
                <Button style={styles.text} title='Aceptar'/>
            </View>

        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems:'center',
        justifyContent:'center',
        //backgroundColor:'#003f5c',
    }

})

export default Register