import React from 'react';

import { 
  StyleSheet,
  TouchableOpacity,
  ScrollView,
  ActivityIndicator,
  Button, 
  View, 
  Text, 
  Image 
} from 'react-native';

import MenuButton from '../components/MenuButton';


const imgSize=150;
const styles = StyleSheet.create({
  box: {
    margin: 5,
    height:190,
    alignItems: 'center',
    //justifyContent: 'center',
    borderRadius: 20,
    padding:5

  },
  title:{
    marginTop:2,
    fontSize:25,
    fontWeight: 'bold',
    color:'#fff'
  },
  img:{
  
    width:imgSize,
    height:imgSize,
    borderRadius:imgSize/2,
  },
  horizontal: {
    flex: 1,
    justifyContent:'center',
    alignItems: 'center',

  },
  azul1: {
    //backgroundColor: 'powderblue',
    backgroundColor: 'powderblue',
  },
  azul2: {
    //backgroundColor: 'skyblue',
    //backgroundColor: '#272121',3c4245,070a0c,1d191a
    backgroundColor: '#070a0c',
  },
});


class HomeScreen extends React.Component {
  constructor(props){
    super(props);
    this.state ={ isLoading: true}
  }
  
  componentDidMount(){

    var req={
      charactersLimit:'characters?limit=100',
      caracterFechaMod:'characters?modifiedSince=2000-01-01',
      cahracterStartWith:'characters?nameStartsWith=Stran',
      comics:'comics?format=comic',
      seriesLimit:'series?orderBy=startYear&limit=100'
    }
    return fetch('http://gateway.marvel.com/v1/public/'+req.charactersLimit+'&ts=1&apikey=f2d0334fce801ceed9b4ef40565bb7a0&hash=f81d5f65a0952f8e047b7f4cb2f48114')
      .then((response) => response.json())
      .then((responseJson) => {

        this.setState({
          isLoading: false,
          //dataSource: responseJson.movies,
          dataSource: responseJson.data.results,
        }, function(){
          //console.log(responseJson);
        });

      })
      .catch((error) =>{
        console.error(error);
      });
  }

  render() {

    if(this.state.isLoading){
      return(
        <View style={styles.horizontal}>
          <ActivityIndicator size="large" color="#0000ff" />
        </View>
      )
    }

    var Birrerias= this.state.dataSource.map((item)=>{
      if (item.description != ""){ 
        return (
          <TouchableOpacity
            style={[styles.box, styles.azul2]}
            onPress={() => {
                /* 1. Navigate to the Details route with params */
                this.props.navigation.navigate('Birreria', {
                  itemId: item.id,
                  urImg: item.thumbnail.path+'.'+item.thumbnail.extension,
                  name: item.name,
                  description: item.description,
                });
              }}
            key={item.id}
            >
            <Image
              style={styles.img}
              source={{uri: item.thumbnail.path+'.'+item.thumbnail.extension}}
            />  
            <Text style={styles.title}>{item.name}</Text>
      
          </TouchableOpacity>
        );
      } 

    });

    return (
      <ScrollView style={{ flex: 1, backgroundColor: '#022d02' }}>
        <View >
          {Birrerias}
        </View>
      
      </ScrollView>
    );
  }
}

export default HomeScreen;