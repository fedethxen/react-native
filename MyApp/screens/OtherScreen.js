import React from 'react';
import { FlatList, ActivityIndicator, Text, View,Image,StyleSheet  } from 'react-native';

const styles = StyleSheet.create({
  container:{
    alignItems:'center',
    marginBottom:30,
    marginLeft:7,
    marginRight:7,
    borderBottomWidth:0.5
  },
  title:{
    fontSize:25,
    fontWeight: 'bold',
    marginBottom:5
  },
  img:{
    width: 300,
    height: 300,
    marginBottom:20
  },
  descrip:{
    fontSize:20,
    padding:10,
    marginBottom:30
  }
})

export default class FetchExample extends React.Component {

  constructor(props){
    super(props);
    this.state ={ isLoading: true}
  }

  componentDidMount(){

    var req={
      charactersLimit:'characters?limit=100',
      comics:'comics?format=comic',
      seriesLimit:'series?orderBy=startYear&limit=100'
    }
    return fetch('http://gateway.marvel.com/v1/public/'+req.seriesLimit+'&ts=1&apikey=f2d0334fce801ceed9b4ef40565bb7a0&hash=f81d5f65a0952f8e047b7f4cb2f48114')
      .then((response) => response.json())
      .then((responseJson) => {

        this.setState({
          isLoading: false,
          //dataSource: responseJson.movies,
          dataSource: responseJson.data.results,
        }, function(){

        });

      })
      .catch((error) =>{
        console.error(error);
      });
  }

  render(){

    if(this.state.isLoading){
      return(
        <View style={{flex: 1, padding: 20}}>
          <ActivityIndicator/>
        </View>
      )
    }

    return(
      <View style={{flex: 1, paddingTop:20}}>
        <FlatList
          data={this.state.dataSource}
          renderItem={({item}) =>(
            <View style={styles.container}>
              <Text style={styles.title}>{item.title}</Text>
              <Image
                style={styles.img}
                source={{uri: item.thumbnail.path+'.'+item.thumbnail.extension}}
              />
              <Text style={styles.descrip}>
                {item.description}
              </Text>
            </View>
          )}
          keyExtractor = { (item, index) => index.toString() }
          //keyExtractor={({id}, index) => id}
        />
      </View>
    );
  }
}