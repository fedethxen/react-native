import React from 'react';
import { Button, View, Text, StyleSheet } from 'react-native';

import ImageBirreria from '../components/ImageBirreria';

const styles = StyleSheet.create({
  box: {
    marginTop: 5,
    borderWidth:0.5,
    borderColor: 'black',
    //height:220,
    alignItems: 'center',
    justifyContent: 'center',
    padding:5

  },
  desc:{
    fontSize:20,
    fontFamily:'sans-serif-condensed',
    fontStyle:'italic',
    
  },
});


class GeolocationScreen extends React.Component {

  render() {
    
    return (
      <View style={{ flex: 1 }}>
        
        <View style={styles.box}>
          <Text style={styles.desc}>
              Geolocalizacion
          </Text>  
          <Text>{this.props.screenProps.iid}</Text>          
        </View>

      </View>
    );
  }
}


export default GeolocationScreen;