import React from 'react';
import {
    Image,
    Dimensions,
    ScrollView,
    StyleSheet,
    Button,
    View,
    Text
} from 'react-native';

import {
  createStackNavigator,
  NavigationStackScreenComponent,
  NavigationStackScreenProps,
} from 'react-navigation-stack';

import ImageBirreria from '../components/ImageBirreria';

import { createAppContainer } from 'react-navigation';
import { createBottomTabNavigator , createMaterialTopTabNavigator} from 'react-navigation-tabs';

import { Ionicons } from '@expo/vector-icons';
//import StackNav from '../navigation/StackNav';
import TopTabNav from '../navigation/TabInScreen';


const TabLayout = createAppContainer(TopTabNav);


class BirreriaScreen extends React.Component {
    constructor(props){
        super(props);
        this.state ={ isLoading: true}
      }

    static navigationOptions = ({ navigation }) => {
        return {
            headerTitle: navigation.getParam('name', 'Nombre Birreria'),

            //headerShown:false,
            headerBackTitleVisible:true,
        };
      };

    componentDidMount() {
      console.log("NAV: ", this.props.navigation);
    }
    
    
    render() {

        const { navigation } = this.props;

        return (
            <React.Fragment>      
                <ImageBirreria
                    url={navigation.getParam('urImg', 'some default value')}
                />
         
                <TabLayout screenProps={
                  {
                    iid: navigation.getParam('itemId', 'NO-ID'),
                    url: navigation.getParam('urImg', 'some default value'),
                    nombre:navigation.getParam('name', 'some default value'),
                    desc:navigation.getParam('description', 'some default value'),
                  }
                }/>
            </React.Fragment>

                    

        );
    }
}



export default BirreriaScreen;