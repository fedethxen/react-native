import React from 'react';
import { Button, View, Text, StyleSheet } from 'react-native';

import ImageBirreria from '../components/ImageBirreria';

const styles = StyleSheet.create({
  box: {
    marginTop: 5,
    borderWidth:0.5,
    borderColor: 'black',
    //height:220,
    alignItems: 'center',
    justifyContent: 'center',
    padding:5

  },
  desc:{
    fontSize:20,
    fontFamily:'sans-serif-condensed',
    fontStyle:'italic',
    
  },
});


class PromotionScreen extends React.Component {

  render() {
    /* 2. Get the param, provide a fallback value if not available */
    const { navigation } = this.props;

    const otherParam = navigation.getParam('otherParam', 'some default value');
    const itemId = navigation.getParam('itemId', 'NO-ID');
    const urImg = navigation.getParam('urImg', 'some default value');
    const name= navigation.getParam('name', 'some default value');
    const description= navigation.getParam('description', 'some default value');

    return (
      <View style={{ flex: 1 }}>
        {/*<ImageBirreria
          url={urImg}
        />*/}
        
        <View style={styles.box}>
          <Text style={styles.desc}>
              Promociones
          </Text>            
        </View>

      </View>
    );
  }
}


export default PromotionScreen;