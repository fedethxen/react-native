import React from 'react';
import { Button, View, Text, StyleSheet } from 'react-native';

import ImageBirreria from '../components/ImageBirreria';

const styles = StyleSheet.create({
  box: {
    marginTop: 5,
    borderWidth:0.5,
    borderColor: 'black',
    //height:220,
    alignItems: 'center',
    justifyContent: 'center',
    padding:5

  },
  desc:{
    fontSize:20,
    fontFamily:'sans-serif-condensed',
    fontStyle:'italic',
    
  },
});


class DetailsScreen extends React.Component {
  static navigationOptions = ({ navigation }) => {
    return {
        headerTitle: navigation.getParam('name', 'Nombre Birreria'),

        //headerShown:false,
        //headerBackTitleVisible:true,
    };
  };

  componentDidMount() {
    console.log("DETAILSCREEN: ", this.props)
  }

  render() {
    /* 2. Get the param, provide a fallback value if not available */

    //const description= this.props.navigation.getParam('description', 'hasgdhaghsdghasdgasgdas');

    return (
      <View style={{ flex: 1 }}>

        
        <View style={styles.box}>
          <Text style={styles.desc}>
              {this.props.screenProps.desc}
          </Text>            
        </View>

      </View>
    );
  }
}


export default DetailsScreen;