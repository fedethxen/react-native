## Algunos Setups
* [Instalaciones Previas](#instalaciones-previas)
* [NODE.JS - Dependencias](#nodejs-dependencias)
* [Postman](#postman)
* [React JS](#react-js)
* [Algunos Comandos GIT](#comandos-git)
* [Algunos Comandos NPM](#comandos-npm)
* [Creacion y Edicion de README.md ](#sobre-readme)


## Instalaciones Previas

* Mongo DB:

    Tiene que ser la version 'Community Edition'

  * Linux : https://docs.mongodb.com/manual/tutorial/install-mongodb-on-ubuntu/
  * Windows: Seguir Video Clase 58   

--CREANDO LA BASE DE DATOS:

* Mostrar DBs creadas:

```
>show dbs

admin   0.000GB
config  0.000GB
local   0.000GB
```

* Crear db:

```
> use pruebas1
switched to db pruebas1
```

* Crear COLECCION (Necesaria para que se vea reflejada la DB):

```
> db.usuarios.save({nombre:'Federico Grosso',mail:'fedegrosso88@gmail.com'})
WriteResult({ "nInserted" : 1 })
> show dbs
admin     0.000GB
config    0.000GB
local     0.000GB
pruebas1  0.000GB
```
* Ver colecciones de la DB:

```
> show collections
usuarios
```

* Ver campos de la colleccion:

```
> db.usuarios.find()
{ "_id" : ObjectId("5d93dcf653fcd9459d4584aa"), "nombre" : "Federico Grosso", "mail" : "fedegrosso88@gmail.com" }
```
<br />


* Robot 3t:

    GUI Para MONGO DB 

    Descargar desde: https://robomongo.org/download


* Node JS:

    https://nodejs.org/es/

    Instalar las version LTS


## Nodejs Dependencias

<p><font  color="red">IMPORTANTE:</font></p>

<p><strong>Si ya se instalaron las dependencias en el proyecto no hace falta instalarlas de vuelta una vez descargadas de GIT</strong></p>

Todos los comandos se ejecutan dentro del directorio '/Backend'.


* Primero hay que generar el archivo 'package.json' :

```
$ npm init
```


* express:

```
$ npm install --save express
```

Es un framework que funciona sobre NODE JS que nos permites generar o trabajar con diferentes rutas que funcionan sobre el protocolo HTTP, nos permite recibir peticiones HTTP y generar las respuestas



* body-parser:

```
$ npm install --save body-parser
```

Cuando envie un form lo convierte en un JSON, un objeto usable por JavaScript



* mongoose:

```
$ npm install --save mongoose
```

Es un ORM para utilizar con MONGO DB, nos da una interfaz de abstraccion para crear diferentes modelos de la DB , metodos, clases para implementar CRUDs.



* connect-multiparty

```
$ npm install --save connect-multiparty
```

libreria que permite procesar el upload de archivos

* nodemon

```
$ npm install --save-dev nodemon
```
libreria que permite reiniciar el servidor automaticamente en cada cambio que se realice


ATENCION: SI sale error en <strong>Windows</strong> despues de 'pullear' reparar la instalacion de la dependencia:

```
$ npm install -g nodemon 
```



## Postman

Postman es un Cliente REST, permite manipular todas las peticiones que yo haga contra mi API RESTful

*Descarga: https://www.getpostman.com/



## React JS

<p><font  color="red">IMPORTANTE:</font></p>

<p><strong>Despues de instalar REACT siguiendo los pasos a continuacion y estando en pleno desarrollo, si se va pullear con GIT en un nuevo entorno de desarrollo POR PRIMERA VEZ (otra pc) se va a tener que instalar de vuelta debido a que toda la carpeta 'AprendiendoReact' contiene un .gitignore en el raiz y evita subir modulos de react (especificamente el directorio: '/node-modules') que por ejemplo pesan 157 MB en total contra 4 MB que suben realmente.<br>RECORDAR: Solo para la primera vez en el entorno nuevo!!</strong></p>

Actualizo la la ultima version de NPM para luego tener la ultima version de REACT:
```
$ npm install -g npm@latest
```
Borro la cache NPM para prevenir que no se descaerge versiones antiguas de REACT:
```
$ npm cache clean --force
```

* CREATE-REACT-PACK:

**Es un paquete de facebook, modulo de node  e interprete de consola que va a permitir instalar REACT facilmente y ademas incluye webpack para la compilacion y minificacion del proyecto, ademas incluye un 'live-reload' para que al cambiar el codigo refresque pantalla automaticamente, prearmado de ficheros con estructura basica de proyecto de react, perimite hacer paquetes de produccion,etc**

**Es el equivalente a ANGULAR CLI en ANGULAR**

* INSTALACION:
```
$ npm install -g create-react-app
```

En LINUX:  
```
$sudo npm install -g create-react-app
```

* CREANDO EL PROYECTO REACT:

En el directorio raiz del proyecto principal:
```
$ create-react-app aprendiendoreact
```
Luego de esto se puede volver a poner las mayuscuscular a '/AprendiendoReact'


* INICIAR SERVIDOR:
```
$ cd AprendiendoReact
$ npm start
```
Navegar: http://localhost:3000


APUNTES:

* Componentes: 
Hay 2 tipos: 
        * CON ESTADO (STATE): Son mas complejos, e.g render() tienen ciclos de vida etc
        * FUNCIONALES SIN ESTADO 



* Errores y soluciones:

-Error 174: 
```
events.js:174
      throw er; // Unhandled 'error' event
      ^

Error: ENOSPC: System limit for number of file watchers reached
```
SOLUCION: Posiblemente colapso de fondo con VISUAL STUDIO CODE,cerrarlo,npm start y listo..volver a abrir VSC

-Error 173:

internal/fs/watchers.js:173
    throw error;
    ^

Error: ENOSPC: System limit for number of file watchers reached, watch '/var/www/html/testNative/MyApp/node_modules/map-cache'.......

SOLUCION:

```
echo fs.inotify.max_user_watches=524288 | sudo tee -a /etc/sysctl.conf && sudo sysctl -p
```




## Comandos GIT

* **Modos de agregar cambios al STAGE**

```
$ git add -A
```
*stages all changes*


```
$ git add .
```
*stages new files and modifications, without deletions*


```
$ git add -u
```
*stages modifications and deletions, without new files*

```
git fetch
```      
*git fetch will download all the recent changes, but it will not put it in your current checked out code (working area)*


```
git checkout origin/master -- path/to/file
```
*git checkout origin/master -- path/to/file will checkout the particular file from the the downloaded changes (origin/master).*


**Deshacer lo viejo en local y traer version acualizada**

```
git fetch origin
git reset --hard origin/master
```
si quieres deshacer todos los cambios locales y commits, puedes traer la última versión del servidor y apuntar a tu copia local principal de esta forma


## Comandos NPM

**Listar dependencias instaladas EN EL PROYECTO sin ramificacion:**
```
npm list --depth=0
```

**Listar dependencias GLOBALES instaladas sin ramificacion:**
```
npm list -g --depth=0
```

**Filtrar por paquetes:**
```
npm list -g --depth=0 | grep <module_name>
```

**Ver todas las versiones disponibles (remotas) para un módulo en particular:**
```
npm view <module_name> versions
```

**Para la última versión remota (Nota, version es singular.):**
```
npm view <module_name> version  
```

**Para saber qué paquetes necesitan actualizarse, puede usar:**
```
npm outdated -g --depth=0
```

**Para actualizar paquetes globales, puedes usar:**
```
npm update -g <package>
```

**Para actualizar todos los paquetes globales, puede utilizar:**
```
npm update -g
```


## Sobre README

https://bulldogjob.com/news/449-how-to-write-a-good-readme-for-your-github-project

https://medium.com/swlh/how-to-make-the-perfect-readme-md-on-github-92ed5771c061